package info.movies.retrofit.model;

import com.google.gson.annotations.SerializedName;

public class Movie {
    @SerializedName("title")
    private String title;
    @SerializedName("movieDescription")
    private String movieDescription;
    @SerializedName("releaseDate")
    private String releaseDate;
    @SerializedName("voteAverage")
    private Double voteAverage;

    public Movie(String title, String movieDescription, String releaseDate, Double voteAverage) {
        this.title = title;
        this.movieDescription = movieDescription;
        this.releaseDate = releaseDate;
        this.voteAverage = voteAverage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMovieDescription() {
        return movieDescription;
    }

    public void setMovieDescription(String movieDescription) {
        this.movieDescription = movieDescription;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Double getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(Double voteAverage) {
        this.voteAverage = voteAverage;
    }
}
