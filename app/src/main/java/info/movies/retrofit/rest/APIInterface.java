package info.movies.retrofit.rest;

import java.util.List;

import info.movies.retrofit.model.Movie;
import info.movies.retrofit.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Header;


public interface APIInterface {

    @GET("/")
    Call<User> authenticateUser(@Header("Authorization") String authkey);

    @GET("movies")
    Call<List<Movie>> getAllMovies(@Header("Authorization") String authkey);

    @POST("users")
    Call<User> submitUser(@Body User newUser);

    @POST("movies")
    Call<Movie> submitMovie(@Header("Authorization") String authKey, @Body Movie newMovie);

}
