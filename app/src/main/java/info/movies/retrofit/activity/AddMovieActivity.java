package info.movies.retrofit.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import info.movies.retrofit.R;
import info.movies.retrofit.model.Movie;
import info.movies.retrofit.rest.APIInterface;
import info.movies.retrofit.rest.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Mircea on 1/10/2018.
 */

public class AddMovieActivity extends AppCompatActivity {

    private static final String TAG = AddMovieActivity.class.getSimpleName();

    @Bind(R.id.addMovieText)
    EditText addMovieText;
    @Bind(R.id.movieTitle)
    TextInputEditText _movieTitle;
    @Bind(R.id.movieDescription)
    TextInputEditText _movieDescription;
    @Bind(R.id.movieReleaseDate)
    TextInputEditText _movieReleaseDate;
    @Bind(R.id.movieAverageRating)
    TextInputEditText _movieAverageRating;
    @Bind(R.id.addMovieButton)
    Button _addMovieButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_movie);
        ButterKnife.bind(this);

        addMovieText.setInputType(InputType.TYPE_NULL);

        _addMovieButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                submitMovie();
            }
        });
        ;
    }


    public void submitMovie() {
        Log.d(TAG, "Adding a new movie process has begun!");

        if (!validateMovieDetails()) {
            movieSubmissionFail();
            return;
        }

        _addMovieButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(AddMovieActivity.this,
                R.style.Theme_AppCompat_Light_DarkActionBar);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Submitting movie now...");
        progressDialog.show();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        final Bundle bundle = getIntent().getExtras();
                        final String encodedCredentials = bundle.getString("Credentials");

                        final String movieTitle = _movieTitle.getText().toString();
                        final String movieDescription = _movieDescription.getText().toString();
                        final String movieReleaseDate = _movieReleaseDate.getText().toString();
                        final Double movieAverageRating = Double.parseDouble(_movieAverageRating.getText().toString());

                        final APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
                        Movie newMovie = new Movie(movieTitle, movieDescription, movieReleaseDate, movieAverageRating);
                        final Call<Movie> call = apiService.submitMovie(encodedCredentials, newMovie);
                        call.enqueue(new Callback<Movie>() {
                            @Override
                            public void onResponse(Call<Movie> inputMovie, retrofit2.Response<Movie> response) {
                                if (response.code() == 200) {
                                    if (response.body() != null) {
                                        Movie addedUser = response.body();
                                        if (addedUser.getTitle().equals(movieTitle) && addedUser.getMovieDescription().equals(movieDescription)) {
                                            movieSubmissionSuccess(encodedCredentials);
                                        }
                                    }
                                } else {
                                    Log.e(TAG, "HTTP Status code: " + String.valueOf(response.code()));
                                    Log.e(TAG, "HTTP Response body: " + String.valueOf(response.body()));
                                    Log.e(TAG, "Stack trace: " + Log.getStackTraceString(new Exception()));
                                    movieSubmissionFail();
                                }
                            }

                            @Override
                            public void onFailure(Call<Movie> call, Throwable t) {
                                // Log error here since request failed
                                Log.e(TAG, t.toString());
                            }
                        });
                        // On complete call either onLoginSuccess or movieSubmissionFail

                        // movieSubmissionFail();
                        progressDialog.dismiss();
                    }
                }, 1000);

    }

    public boolean validateMovieDetails() {
        boolean valid = true;

        final String movieTitle = _movieTitle.getText().toString();
        final String movieDescription = _movieDescription.getText().toString();
        final String movieReleaseDate = _movieReleaseDate.getText().toString();

        if (movieTitle.isEmpty()) {
            _movieTitle.setError("The title can't be left empty!");
            valid = false;
        } else {
            _movieTitle.setError(null);
        }

        if (movieDescription.isEmpty()) {
            _movieDescription.setError("The description can't be left empty!");
            valid = false;
        } else {
            _movieDescription.setError(null);
        }

        if (movieReleaseDate.isEmpty()) {
            _movieReleaseDate.setError("The release date can't be left empty!");
            valid = false;
        } else {
            _movieReleaseDate.setError(null);
        }

        if (_movieAverageRating.getText() == null) {
            _movieAverageRating.setError("The average rating can't be left empty!");
            valid = false;
        } else {
            _movieAverageRating.setError(null);
        }

        return valid;
    }

    public void movieSubmissionSuccess(String encodedCredentials) {
        _addMovieButton.setEnabled(true);
        final Intent myIntent = new Intent(this, MoviesActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        myIntent.putExtra("Credentials", encodedCredentials);
        startActivity(myIntent);
    }

    public void movieSubmissionFail() {
        Toast.makeText(getBaseContext(), "Movie could not be submitted!", Toast.LENGTH_LONG).show();
        _addMovieButton.setEnabled(true);
    }

}
