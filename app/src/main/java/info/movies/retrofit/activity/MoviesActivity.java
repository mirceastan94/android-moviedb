package info.movies.retrofit.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import info.movies.retrofit.R;
import info.movies.retrofit.adapter.MoviesAdapter;
import info.movies.retrofit.model.Movie;
import info.movies.retrofit.rest.ApiClient;
import info.movies.retrofit.rest.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mircea on 1/10/2018.
 */

public class MoviesActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    String encodedCredentials;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movieslist);

        final Bundle bundle = getIntent().getExtras();
        encodedCredentials = bundle.getString("Credentials");

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.movies_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        final Spinner spinner = (Spinner) findViewById(R.id.info_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(MoviesActivity.this.getApplicationContext(), R.array.information_sort, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) view).setTextColor(Color.RED);
                final String selectedItem = parent.getItemAtPosition(position).toString();
                final APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

                final Call<List<Movie>> call = apiService.getAllMovies(encodedCredentials);
                call.enqueue(new Callback<List<Movie>>() {
                    @Override
                    public void onResponse(Call<List<Movie>> call, Response<List<Movie>> response) {
                        int statusCode = response.code();
                        final List<Movie> movies = response.body();

                        switch (selectedItem) {
                            case "Sort by title": {
                                Collections.sort(movies, new Comparator<Movie>() {
                                    @Override
                                    public int compare(Movie movie1, Movie movie2) {
                                        return movie1.getTitle().compareTo(movie2.getTitle());
                                    }
                                });
                                break;
                            }
                            case "Sort by release date": {
                                Collections.sort(movies, Collections.reverseOrder(new Comparator<Movie>() {
                                    @Override
                                    public int compare(Movie movie1, Movie movie2) {
                                        return movie1.getReleaseDate().compareTo(movie2.getReleaseDate());
                                    }
                                }));
                                break;
                            }
                            case "Sort by average rating": {
                                Collections.sort(movies, Collections.reverseOrder(new Comparator<Movie>() {
                                    @Override
                                    public int compare(Movie movie1, Movie movie2) {
                                        return Double.compare(movie1.getVoteAverage(), movie2.getVoteAverage());
                                    }
                                }));
                                break;
                            }
                            default: {
                                Log.e(TAG, "No item selected!");
                            }
                        }
                        recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
                    }

                    @Override
                    public void onFailure(Call<List<Movie>> call, Throwable t) {
                        // Log error here since request failed
                        Log.e(TAG, t.toString());
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void addNewMovie(View view) {
        final Intent myIntent = new Intent(this, AddMovieActivity.class);
        myIntent.putExtra("Credentials", encodedCredentials);
        startActivity(myIntent);
    }

    public void logOut(View view) {
        final Intent myIntent = new Intent(this, MainActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(myIntent);
    }
}
