package info.movies.retrofit.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.Bind;

import info.movies.retrofit.model.User;
import info.movies.retrofit.rest.APIInterface;
import info.movies.retrofit.rest.ApiClient;
import okhttp3.Credentials;

import info.movies.retrofit.R;
import retrofit2.Call;
import retrofit2.Callback;


/**
 * Created by Mircea on 1/10/2018.
 */

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int REQUEST_SIGNUP = 0;

    @Bind(R.id.input_username)
    EditText _usernameText;
    @Bind(R.id.input_password)
    EditText _passwordText;
    @Bind(R.id.loginButton)
    Button _loginButton;
    @Bind(R.id.registerTextView)
    TextView _registerLink;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                loginUser();
            }
        });

        _registerLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
            }
        });
    }

    public void loginUser() {
        Log.d(TAG, "Login process has begun!");

        if (!validateCredentials()) {
            onLoginFail();
            return;
        }

        _loginButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this,
                R.style.Theme_AppCompat_Light_DarkActionBar);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Signing in now...");
        progressDialog.show();

        final String inputUser = _usernameText.getText().toString();
        final String inputPassword = _passwordText.getText().toString();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        final String encodedCredentials = Credentials.basic(inputUser, inputPassword);
                        final APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
                        final Call<User> call = apiService.authenticateUser(encodedCredentials);
                        call.enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> inputUser, retrofit2.Response<User> response) {
                                if (response.code() == 200) {
                                    _usernameText.setText("");
                                    _passwordText.setText("");
                                    onLoginSuccess(encodedCredentials);
                                } else {
                                    Log.e(TAG, String.valueOf(response.code()));
                                    onLoginFail();
                                }
                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {
                                Log.e(TAG, t.toString());
                            }
                        });

                        progressDialog.dismiss();
                    }
                }, 1000);
    }

    @Override
    public void onBackPressed() {
        // disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess(String encodedCredentials) {
        _loginButton.setEnabled(true);
        final Intent myIntent = new Intent(this, MoviesActivity.class);
        myIntent.putExtra("Credentials", encodedCredentials);
        startActivity(myIntent);
    }

    public void onLoginFail() {
        Toast.makeText(getBaseContext(), "Login has failed!", Toast.LENGTH_LONG).show();
        _loginButton.setEnabled(true);
    }

    public boolean validateCredentials() {
        boolean valid = true;

        final String username = _usernameText.getText().toString();
        final String password = _passwordText.getText().toString();

        if (username.isEmpty() || username.length() < 4 || username.length() > 16) {
            _usernameText.setError("The username must contain between 4 and 12 alphanumeric characters!");
            valid = false;
        } else {
            _usernameText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 12) {
            _passwordText.setError("The password must contain between 4 and 12 alphanumeric characters!");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

}
